
# -*- coding: utf-8 -*-
module Sibsimer
  module FileType
    class Base
      require 'test/unit'
      include Test::Unit::Assertions
      
    end

    module XML
    end
    module TSV
    end

    class Config
      include XML
    end
    
    class Pre
      include TSV
    end
    

  end
end
