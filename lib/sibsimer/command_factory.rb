# -*- coding: utf-8 -*-
module Sibsimer
  module CommandFactory
    require 'test/unit'

    module Base
      def factory(arg)
        raise "abstract method"
      end

    end

    class Template
      include Base
      include Test::Unit::Assertions
      attr_accessor :config_name

      # def initialize()
      # end

      def produce
        Sibsimer::Command::Template.new()
      end
    end
    
    
  end
end


