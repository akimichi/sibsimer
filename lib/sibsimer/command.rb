# -*- coding: utf-8 -*-
module Sibsimer
  module Command
    require 'open3'
    require 'test/unit'
    require 'nokogiri'

    class Base
      attr_accessor :content

      def execute
        stdout, stderr, status = Open3.capture3(@content)
        if status.exitstatus == 0
          stdout
        else
          STDERR.puts "There was a problem running '#{@content}'"
          STDERR.puts stderr
          exit -1
        end
      end
    end

    class Template < Base
      attr_accessor :content

      def initialize()
        @content = "sibsim --template"
      end

      def execute(prefix) # Model::Config
        config_content = super()
        xml_doc = Nokogiri::XML(config_content) do |config|
          config.noblanks
          # config.options = Nokogiri::XML::ParseOptions.STRICT | Nokogiri::XML::ParseOptions.NOBLANKS
        end
        Model::Config.new(xml_doc)
      end
    end

    class Simulate < Base
      attr_accessor :content

      def initialize()
        @content = "sibsim "
      end

      def execute(config) # Model::Config => Model::LinkagePre
        config_content = super()
        
      end
    end
  end
end
