# -*- coding: utf-8 -*-
module Sibsimer
  module Model
    require 'open3'
    require 'test/unit'
    require 'nokogiri'

    class Base
    end

    class Config < Base
      attr_accessor :content
      def initialize(xml)
        @content = xml
      end
    end

    class LinkagePre < Base
    end
  end
end

