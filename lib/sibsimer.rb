require 'sibsimer/version.rb'

# Add requires for other files you add to your project here, so
# you just need to require this one file in your bin file

require 'sibsimer/command.rb'
require 'sibsimer/command_factory.rb'
require 'sibsimer/file_type.rb'
require 'sibsimer/model.rb'
