# -*- coding: utf-8 -*-

require "sibsimer"

describe Sibsimer::CommandFactory do
  include Sibsimer::CommandFactory

  before do

  end

  describe Sibsimer::CommandFactory::Template do 
    it "produce sibsim template command" do
      config_name = "sample"
      factory = Sibsimer::CommandFactory::Template.new()
      expect(factory).to be_a Sibsimer::CommandFactory::Template
      # expect(factory.config_name).to eq(config_name)
      command = factory.produce
      expect(command).to be_a Sibsimer::Command::Template
      expect(command.content).to eq("sibsim --template")
    end
  end
end
  
