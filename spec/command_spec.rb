# -*- coding: utf-8 -*-

require "sibsimer"
require 'nokogiri'


describe Sibsimer::Command do
  include Sibsimer::Command


  describe Sibsimer::Command::Template do 
    before do
      @prefix = "sample"
      @template_command = Sibsimer::Command::Template.new() 
    end
    it "execute template command returns xml instance" do
      config = @template_command.execute(@prefix)
      expect(config).to be_a Sibsimer::Model::Config
      # expect(sample_xml).to be_a Nokogiri::XML::Document
      # p sample_xml
      expect(config.content.xpath('//sibsim/Genotype[@id="chromosome"]')[0].name).to eq("Genotype")
    end
  end
end
  
